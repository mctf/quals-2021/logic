package ru.serega6531;

import ru.serega6531.parser.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BinaryFormatReader {

    private final DataInputStream is;
    private final Map<Integer, BooleanObject> objects = new HashMap<>();

    public BinaryFormatReader(File file) throws FileNotFoundException {
        is = new DataInputStream(new FileInputStream(file));
    }

    public BooleanObject read() throws IOException {
        final int objectsAmount = is.readUnsignedShort();
        for (int i = 0; i < objectsAmount; i++) {
            final BooleanObject object = readObject();
            objects.put(object.getId(), object);
        }

        for (BooleanObject o : objects.values()) {
            o.setParent(objects.get(o.getParent().getId()));

            if (o instanceof Operator) {
                Operator operator = (Operator) o;
                operator.setInputs(
                        operator.getInputs().stream()
                                .map(i -> objects.get(i.getId()))
                                .collect(Collectors.toList())
                );
            }
        }

        return objects.values().stream()
                .filter(o -> o.getParent() == null)
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Root not found"));
    }

    private BooleanObject readObject() throws IOException {
        int id = is.readUnsignedShort();
        BooleanObjectType type = BooleanObjectType.values()[is.readUnsignedByte()];
        int parentId = is.readUnsignedShort();
        BooleanObject result;

        switch (type) {
            case CONSTANT:
                result = new ConstantValue(is.readBoolean());
                break;
            case VARIABLE:
                int length = is.readUnsignedByte();
                byte[] buf = new byte[length];
                is.readFully(buf);
                result = new Variable(new String(buf));
                break;
            case OPERATOR:
                OperatorType operatorType = OperatorType.values()[is.readUnsignedByte()];
                int inputsAmount = is.readUnsignedByte();
                List<BooleanObject> inputs = new ArrayList<>();

                for (int i = 0; i < inputsAmount; i++) {
                    inputs.add(new TempObject(is.readUnsignedShort()));
                }

                result = new Operator(operatorType, inputs);
                break;
            default:
                throw new IllegalStateException();
        }

        result.setId(id);
        result.setParent(new TempObject(parentId));
        return result;
    }

}
