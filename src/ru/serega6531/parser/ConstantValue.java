package ru.serega6531.parser;

import java.util.Collections;
import java.util.List;

public class ConstantValue extends BooleanObject {

    private final boolean value;

    public ConstantValue(boolean value) {
        super(BooleanObjectType.CONSTANT);
        this.value = value;
    }

    @Override
    public BooleanObject copy() {
        ConstantValue val = new ConstantValue(value);
        if (getParent() != null) {
            val.setParent(getParent());
        }
        return val;
    }

    @Override
    public List<BooleanObject> getAllObjectsDeep() {
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        return value ? "1" : "0";
    }

    public boolean getValue() {
        return value;
    }
}
