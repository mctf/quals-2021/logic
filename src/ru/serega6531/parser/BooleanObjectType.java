package ru.serega6531.parser;

public enum  BooleanObjectType {
    CONSTANT, VARIABLE, OPERATOR
}
