package ru.serega6531.parser;

import java.util.List;

public abstract class BooleanObject {

    private int id;
    private BooleanObjectType type;
    private BooleanObject parent;

    public BooleanObject(BooleanObjectType type) {
        this.type = type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public BooleanObjectType getType() {
        return type;
    }

    public void setParent(BooleanObject parent) {
        this.parent = parent;
    }

    public BooleanObject getParent() {
        return parent;
    }

    public abstract BooleanObject copy();

    public abstract List<BooleanObject> getAllObjectsDeep();
}
