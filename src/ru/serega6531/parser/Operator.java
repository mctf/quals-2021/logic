package ru.serega6531.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Operator extends BooleanObject {

    private final OperatorType operatorType;
    private List<BooleanObject> inputs;

    public Operator(OperatorType operatorType, List<BooleanObject> inputs) {
        super(BooleanObjectType.OPERATOR);
        this.operatorType = operatorType;
        this.inputs = inputs;
    }

    @Override
    public BooleanObject copy() {
        Operator val = new Operator(operatorType,
                inputs.stream()
                        .map(BooleanObject::copy)
                        .collect(Collectors.toList()));
        val.getInputs().forEach(i -> i.setParent(val));
        if (getParent() != null) {
            val.setParent(getParent());
        }
        return val;
    }

    @Override
    public List<BooleanObject> getAllObjectsDeep() {
        List<BooleanObject> list = new ArrayList<>();
        list.add(this);
        inputs.forEach(i -> list.addAll(i.getAllObjectsDeep()));
        return list;
    }

    @Override
    public String toString() {
        if(operatorType == OperatorType.NOT) {
            return String.format("(not %s)", inputs.get(0).toString());
        } else {
            return String.format("(%s %s %s)",
                    inputs.get(0).toString(),
                    operatorType.name().toLowerCase(),
                    inputs.get(1).toString());
        }
    }

    public OperatorType getOperatorType() {
        return operatorType;
    }

    public List<BooleanObject> getInputs() {
        return inputs;
    }

    public void setInputs(List<BooleanObject> inputs) {
        this.inputs = inputs;
    }
}
