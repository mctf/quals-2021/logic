package ru.serega6531.parser;

import java.util.List;

public class TempObject extends BooleanObject {

    public TempObject(int id) {
        super(BooleanObjectType.CONSTANT);
        setId(id);
    }

    @Override
    public BooleanObject copy() {
        return null;
    }

    @Override
    public List<BooleanObject> getAllObjectsDeep() {
        return null;
    }
}
