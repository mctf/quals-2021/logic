package ru.serega6531.parser;

import java.util.Collections;
import java.util.List;

public class Variable extends BooleanObject {

    private final String name;

    public Variable(String name) {
        super(BooleanObjectType.VARIABLE);
        this.name = name;
    }

    @Override
    public BooleanObject copy() {
        return new Variable(name);
    }

    @Override
    public List<BooleanObject> getAllObjectsDeep() {
        return Collections.singletonList(this);
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }
}
