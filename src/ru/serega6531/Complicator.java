package ru.serega6531;

import ru.serega6531.parser.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Complicator {

    private final Random rand = ThreadLocalRandom.current();

    private List<BooleanObject> objects;
    private BooleanObject top;

    public Complicator(BooleanObject top) {
        this.top = top;
        objects = top.getAllObjectsDeep();
    }

    public BooleanObject complicate(int iterations) {
        for (int i = 0; i < iterations; i++) {
            OperatorType type = rand.nextBoolean() ? OperatorType.AND : OperatorType.OR;

            final int choice = rand.nextInt(4);
            switch (choice) {
                case 0:
                    applyIdempotent(type);
                    break;
                case 1:
                    applyDistributive(type);
                    break;
                case 2:
                    applyConstant(type);
                    break;
                case 3:
                    applyExcludedMiddle(type);
                    break;
            }

            objects = top.getAllObjectsDeep().stream()
                    .distinct()
                    .collect(Collectors.toList());
        }

        return top;
    }

    private void applyIdempotent(OperatorType type) {
        final BooleanObject obj = objects.get(rand.nextInt(objects.size()));

        // A = A * A

        final BooleanObject copy = obj.copy();
        replaceObjectWithOperator(type, obj, copy);
    }

    private void applyDistributive(OperatorType type) {
        final OperatorType opposite = type == OperatorType.AND ? OperatorType.OR : OperatorType.AND;
        final List<BooleanObject> filtered = findObjects(o -> {
            if (!(o instanceof Operator)) {
                return false;
            }

            Operator operator = (Operator) o;
            if (operator.getOperatorType() != type) {
                return false;
            }

            final BooleanObject left = operator.getInputs().get(0);
            final BooleanObject right = operator.getInputs().get(1);
            return ((left instanceof Operator && ((Operator) left).getOperatorType() == opposite) ||
                    (right instanceof Operator && ((Operator) right).getOperatorType() == opposite));
        });

        if (filtered.isEmpty()) {
            return;
        }

        final Operator obj = (Operator) filtered.get(rand.nextInt(filtered.size()));

        // other * (insideLeft + insideRight) = (other * insideLeft) + (other * insideRight)

        boolean left = obj.getInputs().get(0) instanceof Operator && ((Operator) obj.getInputs().get(0)).getOperatorType() == opposite;
        final BooleanObject parent = obj.getParent();
        final Operator brackets = (Operator) obj.getInputs().get(left ? 0 : 1);
        final BooleanObject other = obj.getInputs().get(left ? 1 : 0);
        final BooleanObject otherCopy = other.copy();
        final BooleanObject insideLeft = brackets.getInputs().get(0);
        final BooleanObject insideRight = brackets.getInputs().get(1);

        final Operator newLeft = new Operator(type, Arrays.asList(other, insideLeft));
        final Operator newRight = new Operator(type, Arrays.asList(otherCopy, insideRight));
        final Operator newObject = new Operator(opposite, Arrays.asList(newLeft, newRight));

        other.setParent(newLeft);
        insideLeft.setParent(newLeft);

        otherCopy.setParent(newRight);
        insideRight.setParent(newRight);

        newLeft.setParent(newObject);
        newRight.setParent(newObject);

        replaceObject(obj, parent, newObject);
    }

    private void applyConstant(OperatorType type) {
        final BooleanObject obj = objects.get(rand.nextInt(objects.size()));

        // A = A and 1
        // A = A or 0

        final BooleanObject constant = new ConstantValue(type == OperatorType.AND);
        replaceObjectWithOperator(type, obj, constant);
    }

    private void applyExcludedMiddle(OperatorType type) {
        boolean value = type == OperatorType.OR;

        final List<BooleanObject> filteredConstants = findObjects(o -> {
            if (!(o instanceof ConstantValue)) {
                return false;
            }

            ConstantValue constant = (ConstantValue) o;
            return constant.getValue() == value;
        });

        if (filteredConstants.isEmpty()) {
            return;
        }

        // 1 = A or (not A)
        // 0 = A and (not A)

        final ConstantValue constant = (ConstantValue) filteredConstants.get(rand.nextInt(filteredConstants.size()));
        final BooleanObject parent = constant.getParent();

        final List<BooleanObject> filteredVariables = findObjects(Variable.class::isInstance);

        final Variable variable = (Variable) filteredVariables.get(rand.nextInt(filteredVariables.size())).copy();
        final Operator inverted = new Operator(OperatorType.NOT, Collections.singletonList(variable));
        final Operator newObject = new Operator(type, Arrays.asList(variable, inverted));

        variable.setParent(newObject);
        inverted.setParent(newObject);

        replaceObject(constant, parent, newObject);
    }

    private void replaceObjectWithOperator(OperatorType type, BooleanObject obj, BooleanObject replace) {
        BooleanObject parent = obj.getParent();
        Operator newObject = new Operator(type, Arrays.asList(obj, replace));
        obj.setParent(newObject);
        replace.setParent(newObject);
        replaceObject(obj, parent, newObject);
    }

    private void replaceObject(BooleanObject obj, BooleanObject parent, Operator replace) {
        replace.setParent(parent);

        if (parent instanceof Operator) {
            Operator operator = (Operator) parent;
            if (operator.getInputs().get(0) == obj) {
                operator.getInputs().set(0, replace);
            } else {
                operator.getInputs().set(1, replace);
            }
        } else if (top == obj) {
            top = replace;
        }
    }

    private List<BooleanObject> findObjects(Predicate<BooleanObject> predicate) {
        return objects.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    public List<BooleanObject> getObjects() {
        return objects;
    }
}
