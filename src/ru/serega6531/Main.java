package ru.serega6531;

import ru.serega6531.parser.BooleanObject;
import ru.serega6531.parser.Parser;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        File outFile = new File("result.bin");
        String equation = "((((a and c) or (b xor c)) and (a xor d)) and (e or ((not b) and c)))";
        Parser parser = new Parser();

        final BooleanObject parsed = parser.parse(equation);
        System.out.println("Parsed: " + parsed.toString());
        Complicator complicator = new Complicator(parsed);
        final BooleanObject complicated = complicator.complicate(500);
//        System.out.println("Complicated: " + complicated.toString());

        int id = 1;
        List<BooleanObject> objects = complicator.getObjects();
        for (BooleanObject object : objects) {
            object.setId(id++);
        }

        Collections.shuffle(objects);
        BinaryFormatWriter.write(objects, outFile);

        // Solution:
        final BooleanObject read = new BinaryFormatReader(outFile).read();
        System.out.println("Read: " + read);

        if(complicated.toString().equals(read.toString())) {
            System.out.println("Совпадает");
        } else {
            System.out.println("Не совпадает");
        }
    }

}
